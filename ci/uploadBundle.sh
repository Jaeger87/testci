#!/usr/bin/env bash

set -e
set -x

echo "Uploading bundles for $BUILD_TARGET"

BACKBLAZE_TOKEN=$(cat backblaze/token.txt)
BACKBLAZE_API_URL=$(cat backblaze/apiURL.txt)
UPLOADURL=$(cat backblaze/uploadURL.txt)
UPLOADTOKEN=$(cat backblaze/uploadToken.txt)
MIME_TYPE=custom/content-type

BundleDirectory=$UNITY_DIR/BuildsBundle

shopt -s globstar

for file in "$BundleDirectory"/**/*; 
do 
  if [ -f "$file" ]; then
    httpcodeanswer=0
    SHA1_OF_FILE=$(openssl dgst -sha1 $file | awk '{print $2;}')
    while [[ $httpcodeanswer -ne '200' ]]; do
      httpcodeanswer=$(curl -o /dev/null -s -w "%{http_code}\n" -H "Authorization:$UPLOADTOKEN" -H "X-Bz-File-Name:Andrea$file" -H "Content-Type:$MIME_TYPE" -H "X-Bz-Content-Sha1:$SHA1_OF_FILE" -H "X-Bz-Info-Author:unknown" -H "X-Bz-Server-Side-Encryption:AES256" --data-binary "@$file" ${UPLOADURL})
    done
  fi
done